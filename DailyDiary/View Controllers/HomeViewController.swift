//
//  HomeViewController.swift
//  DailyDiary
//
//  Created by Huda Jawed on 7/3/19.
//  Copyright © 2019 Huda Jawed. All rights reserved.
//

import UIKit
import DropDown

class HomeViewController: UIViewController {

    @IBOutlet weak var monthView: UIView!
    @IBOutlet weak var noMemoriesLabel: UILabel!
    @IBOutlet weak var addBtn: CustomButton!
    @IBOutlet weak var tableView: UITableView!
    var data = [Dictionary<String,Any>]()
    var month = ""
    var dropDown = DropDown()
    let monthArray = ["January","February","March","April","May","June","July","August","September","October","November","December"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.navigationBar.isHidden = false
        self.configureNavigationBar(title: "Secret Diary", tintColor: .white, barColor: UIColor(named: "PrimaryColor")!, isTransparent: true)
       
        let settingItem = UIBarButtonItem(image: #imageLiteral(resourceName: "setting"), style: .plain, target: self, action: #selector(didTapSettingButton))
        self.navigationItem.leftBarButtonItem = settingItem
        
        let filterItem = UIBarButtonItem(image: #imageLiteral(resourceName: "filter"), style: .plain, target: self, action: #selector(didTapFilterButton))
        self.navigationItem.rightBarButtonItem = filterItem
        
        tableView.delegate = self
        tableView.dataSource = self
        
        DropDown.configureDropDown(dropDown: dropDown, view: self.monthView)
        
        let df = DateFormatter()
        df.dateFormat = "yyyy-MMMM-dd"
        let str = df.string(from: Date())
        let now = str.split(separator: "-")
         month = String(now[1])
         getData(month: month)
        
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
    
        self.configureNavigationBar(title: "Secret Diary", tintColor: .white, barColor: UIColor(named: "PrimaryColor")!, isTransparent: true)
          getData(month: month)
    }
    
    @objc func didTapFilterButton()
    {
        self.dropDown.dataSource = self.monthArray
        self.dropDown.show()
        
        self.dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.getData(month: self.monthArray[index])
          self.month = self.monthArray[index]
          self.dropDown.hide()
        }
    }
    
    
    @objc func didTapSettingButton()
    {
        self.pushVC(identifier: "SettingsViewController")
    }
    
    
    @IBAction func didTapAddButton(_ sender: Any)
    {
        self.pushVC(identifier:"AddPageViewController")
    }
    
    @objc func didTapDeleteButton(_ sender: UIButton)
    {
        let alert = UIAlertController(title: "Delete", message: "Are you sure you want to delete this?", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler:nil))
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
        let cell = sender.superview?.superview?.superview as? HomeTableViewCell
        let row = self.tableView.indexPath(for: cell!)?.row
        DataHandler.sharedInstance.deleteStory(id:(self.data[row!]["id"] as? String) ?? "")
        self.getData(month: self.month)
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func getData(month:String)
    {
        data = [Dictionary<String,Any>]()
        if let data = DataHandler.sharedInstance.getStories(month: month)
        {
            self.data = data
            if self.data.count != 0
            {
                self.tableView.isHidden = false
                self.noMemoriesLabel.isHidden = true
            }
        }
        else
        {
            self.tableView.isHidden = true
        }
           animate()
          self.tableView.reloadData()
    }
    
    func animate()
    {
        self.tableView.alpha = 0.0
        UIView.animate(withDuration: 1.0) {
            self.tableView.alpha = 1.0
        }
    }
    
}


extension HomeViewController: UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
       return self.data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HomeTableViewCell") as? HomeTableViewCell
        cell?.titleLabel.text = self.data[indexPath.row]["title"] as? String
        cell?.detailLabel.text = self.data[indexPath.row]["detail"] as? String
        cell?.moodLabel.text = self.data[indexPath.row]["mood"] as? String
        cell?.timeLabel.text = self.data[indexPath.row]["time"] as? String
        cell?.dateLabel.text = self.data[indexPath.row]["date"] as? String
        cell?.monthLabel.text = self.data[indexPath.row]["month"] as? String
        cell?.yearLabel.text = self.data[indexPath.row]["year"] as? String
        cell?.colorView.backgroundColor = UIColor(hexString: self.data[indexPath.row]["color"] as! String)
        cell?.deleteBtn.addTarget(self, action: #selector(didTapDeleteButton(_:)), for: .touchUpInside)
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AddPageViewController") as? AddPageViewController
        vc?.id = self.data[indexPath.row]["id"] as? String ?? ""
        vc?.titleText =  self.data[indexPath.row]["title"] as? String ?? ""
        vc?.details = self.data[indexPath.row]["detail"] as? String ?? ""
        vc?.mood = self.data[indexPath.row]["mood"] as? String ?? ""
        vc?.color = self.data[indexPath.row]["color"] as? String ?? ""
        vc?.date = (self.data[indexPath.row]["date"] as! String)+" "+(self.data[indexPath.row]["month"] as! String)+" "+(self.data[indexPath.row]["year"] as! String)+", "+(self.data[indexPath.row]["day"] as! String)
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    
}
