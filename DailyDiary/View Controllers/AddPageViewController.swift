//
//  AddPageViewController.swift
//  DailyDiary
//
//  Created by Huda Jawed on 7/3/19.
//  Copyright © 2019 Huda Jawed. All rights reserved.
//

import UIKit
import SwiftyDrop
import STZPopupView
import DropDown
import ALCameraViewController

class AddPageViewController: UIViewController,UIImagePickerControllerDelegate, UINavigationControllerDelegate,ColorDelegate  {
    
    
    @IBOutlet weak var colorBtn: UIButton!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var noImagesLabel: UILabel!
    @IBOutlet weak var dateView: CustomView!
    @IBOutlet weak var titleField: CustomTextField!
    @IBOutlet weak var moodField: CustomTextField!
    @IBOutlet weak var storyTextView: CustomTextView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var saveBtn: UIButton!
    @IBOutlet weak var resetBtn: UIButton!
    @IBOutlet weak var photoBtn: UIButton!
    @IBOutlet weak var voiceTextBtn: UIButton!
    
    
    var dropDown = DropDown()
    var id = ""
    var date = ""
    var titleText = ""
    var mood = ""
    var details = ""
    
    var images = [UIImage]()
    var imageData = [Data]()
    var color = "000000"
    var moodArray = ["🙂 Happy","😇 Blessed","😌 Relieved","😀 Thankful","😎 Cool","😍 Loved", "☹️ Sad","😠 Angry", "🤔 Thoughtful","🤪 Goofy","😟 Worried","😫 Tired","😴 Sleepy","🤒 Sick","😵 Dizzy"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.configureNavigationBar(title: "Secret Diary", tintColor: .white, barColor: UIColor(named: "PrimaryColor")!, isTransparent: true)
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.layer.cornerRadius = 5
        self.collectionView.layer.borderWidth = 1.0
        self.collectionView.layer.borderColor = UIColor.lightGray.cgColor
        self.noImagesLabel.isHidden = false
        
        if id == ""
        {
            let df = DateFormatter()
            df.dateFormat = "yyyy-MMMM-dd"
            let str = df.string(from: Date())
            let now = str.split(separator: "-")
            let date = String(now[2])
            let month = String(now[1])
            let year = String(now[0])
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "EEEE"
            let dayInWeek = dateFormatter.string(from: Date())
            self.dateLabel.text = date+" "+month+" "+year+", "+dayInWeek
        }
        else
        {
            self.colorBtn.isHidden = true
            self.stackView.isHidden = true
            self.dateView.backgroundColor = UIColor(hexString: color, alpha: 0.6)
            self.dateLabel.textColor = .white
            self.dateLabel.text = date
            self.titleField.text = titleText
            self.moodField.text = mood
            self.storyTextView.text = details
            if let imageDic = DataHandler.sharedInstance.getImages(id:self.id)
            {
                for image in imageDic
                {
                    self.images.append(UIImage(data:image["image"] as! Data)!)
                    self.noImagesLabel.isHidden = true
                }
                self.collectionView.reloadData()
            }
        }
        
        DropDown.configureDropDown(dropDown: dropDown, view: self.moodField)
        
        
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        self.configureNavigationBar(title: "Secret Diary", tintColor: .white, barColor: UIColor(named: "PrimaryColor")!, isTransparent: true)
    }
    
    
    @IBAction func didTapAddMood(_ sender: Any) {
        self.dropDown.dataSource = self.moodArray
        self.dropDown.show()
        
        self.dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.moodField.text = self.moodArray[index]
            self.dropDown.hide()
        }
    }
    
    @IBAction func didTapAddImages(_ sender: Any) {
        self.view.endEditing(true)
        self.openCamera()
    }
    
    func openCamera(){
        
        let cameraViewController = CameraViewController { [weak self] image, asset in
           
            if let pickedImage = image {
                
                self!.images.append(pickedImage)
                self!.noImagesLabel.isHidden = true
                let imageData = pickedImage.jpegData(compressionQuality: 0.5)
                self!.imageData.append(imageData!)
                self!.collectionView.reloadData()
                
            }
            
            self?.dismiss(animated: true, completion: nil)
            
        }
        
        present(cameraViewController, animated: true, completion: nil)
    }
    
//    func openGallery() {
//
//        let imagePicker = UIImagePickerController()
//        imagePicker.delegate = self
//        imagePicker.allowsEditing = false
//        imagePicker.sourceType = .photoLibrary
//
//        present(imagePicker, animated: true, completion: nil)
//
//    }
    
    @IBAction func didTapResetBtn(_ sender: Any)
    {
        
        self.titleField.text = ""
        self.moodField.text = ""
        self.storyTextView.text = ""
        self.images = [UIImage]()
        self.imageData = [Data]()
        self.dateView.backgroundColor = .white
        self.color = "000000"
        self.collectionView.reloadData()
    }
    
    @IBAction func didTapSaveButton(_ sender: Any)
    {
        
        DataHandler.sharedInstance.saveStory(title: self.titleField.text!, mood: self.moodField.text!, detail: self.storyTextView.text!,color:self.color, images: self.imageData)
        Drop.down("Saved successfully", state: .success, duration: 1.0)
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func didTapDeleteBtn(_ sender: UIButton)
    {
        let cell = sender.superview?.superview as? AddStoryCollectionViewCell
        let row = self.collectionView.indexPath(for: cell!)?.row
        self.images.remove(at: row!)
        self.imageData.remove(at: row!)
        self.collectionView.reloadData()
        if self.images.count == 0
        {
            self.noImagesLabel.isHidden = false
        }
    }
    
    @IBAction func didTapColorBtn(_ sender: Any)
    {
        let popupView = Bundle.main.loadNibNamed("Color", owner: self, options: nil)?[0] as! Color
        popupView.setNeedsLayout()
        popupView.delegate = self
        self.presentPopupView(popupView)
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]){
        
        var image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        if (!(image != nil))
        {
            image = info[UIImagePickerController.InfoKey.editedImage] as? UIImage
        }
        
        self.images.append(image!)
        let imageData = image?.jpegData(compressionQuality: 0.5)
        self.imageData.append(imageData!)
        dismiss(animated: true, completion: nil)
        self.noImagesLabel.isHidden = true
        self.collectionView.reloadData()
        
    }
    
    func getColor(color: String)
    {
        self.color = color
        self.dateView.backgroundColor = UIColor(hexString: color, alpha: 0.6)
        self.dismissPopupView()
    }
    
    
}


extension AddPageViewController:UICollectionViewDelegate,UICollectionViewDataSource
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.images.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AddStoryCollectionViewCell", for: indexPath) as? AddStoryCollectionViewCell
        
        cell?.imageView.image = self.images[indexPath.row]
        cell?.crossBtn.addTarget(self, action: #selector(didTapDeleteBtn(_:)), for: .touchUpInside)
        if id != ""
        {
            cell?.crossBtn.isHidden = true
        }
        
        return cell!
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ImageViewController") as? ImageViewController
        vc?.image = self.images[indexPath.row]
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    
    
}



