//
//  ImageViewController.swift
//  DailyDiary
//
//  Created by Huda Jawed on 7/5/19.
//  Copyright © 2019 Huda Jawed. All rights reserved.
//

import UIKit

class ImageViewController: UIViewController {

    @IBOutlet weak var storyImageView: UIImageView!
    var image : UIImage?
    override func viewDidLoad() {
        super.viewDidLoad()

       self.storyImageView.image = image
    }
    

   
}
