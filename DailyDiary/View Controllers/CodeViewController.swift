//
//  HomeViewController.swift
//  DailyDiary
//
//  Created by Huda Jawed on 7/3/19.
//  Copyright © 2019 Huda Jawed. All rights reserved.
//

import UIKit
import STZPopupView
import SwiftyDrop

class CodeViewController: UIViewController,Done {
  
    @IBOutlet var y2: UILabel!
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var monthLabel: UILabel!
    @IBOutlet var yearLabel: UILabel!
    @IBOutlet weak var forgotCode: UIButton!
    @IBOutlet weak var zero: UIButton!
    @IBOutlet weak var nine: UIButton!
    @IBOutlet weak var eight: UIButton!
    @IBOutlet weak var seven: UIButton!
    @IBOutlet weak var six: UIButton!
    @IBOutlet weak var five: UIButton!
    @IBOutlet weak var four: UIButton!
    @IBOutlet weak var three: UIButton!
    @IBOutlet weak var two: UIButton!
    @IBOutlet weak var one: UIButton!
    @IBOutlet weak var titleView: CustomView!
    @IBOutlet weak var numberView: CustomView!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var addCodeView: CustomView!
    @IBOutlet weak var firstView: CustomView!
    @IBOutlet weak var secondView: CustomView!
    @IBOutlet weak var thirdView: CustomView!
    @IBOutlet weak var fourthView: CustomView!
    @IBOutlet weak var label: UILabel!
    var code = ""
    var check1 = false
    var check2 = false
    var check3 = false
    var check4 = false
    var color = UIColor(named: "PrimaryColor")!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        if let code = UserDefaults.standard.object(forKey: "code") as? String
        {
            self.addCodeView.isHidden = true
            self.label.isHidden = true
        }
        else
        {
            self.stackView.isHidden = true
            self.numberView.isHidden = true
            self.forgotCode.isHidden = true
        }
        
        let df = DateFormatter()
        df.dateFormat = "yyyy-MMMM-dd"
        let str = df.string(from: Date())
        let now = str.split(separator: "-")
      
        updateColor()
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if let code = UserDefaults.standard.object(forKey: "code") as? String
        {
            self.stackView.isHidden = false
            self.numberView.isHidden = false
            self.forgotCode.isHidden = false
            updateColor()
           
        }
        else
        {
            self.stackView.isHidden = true
            self.numberView.isHidden = true
            self.forgotCode.isHidden = true
            self.addCodeView.isHidden = false
            self.label.isHidden = false
        }
         self.restart()
    }
    
    func updateColor()
    {
        if let bgColor = UserDefaults.standard.object(forKey: "color") as? String
        {
            self.color = UIColor(hexString: bgColor)
            self.titleView.backgroundColor = color
            self.one.setTitleColor(color, for: .normal)
            self.two.setTitleColor(color, for: .normal)
            self.three.setTitleColor(color, for: .normal)
            self.four.setTitleColor(color, for: .normal)
            self.five.setTitleColor(color, for: .normal)
            self.six.setTitleColor(color, for: .normal)
            self.seven.setTitleColor(color, for: .normal)
            self.eight.setTitleColor(color, for: .normal)
            self.nine.setTitleColor(color, for: .normal)
            self.zero.setTitleColor(color, for: .normal)
        }
        else
        {
            self.color = UIColor(named: "PrimaryColor")!
            self.titleView.backgroundColor = color
            self.one.setTitleColor(color, for: .normal)
            self.two.setTitleColor(color, for: .normal)
            self.three.setTitleColor(color, for: .normal)
            self.four.setTitleColor(color, for: .normal)
            self.five.setTitleColor(color, for: .normal)
            self.six.setTitleColor(color, for: .normal)
            self.seven.setTitleColor(color, for: .normal)
            self.eight.setTitleColor(color, for: .normal)
            self.nine.setTitleColor(color, for: .normal)
            self.zero.setTitleColor(color, for: .normal)
        }
        
    }
    
    func updateView()
    {
     if !check1
     {
       check1 = true
        self.firstView.backgroundColor = color
     }
        else if !check2
     {
        check2 = true
        self.secondView.backgroundColor = UIColor(named: "SecondaryColor")!
     }
     else if !check3
     {
        check3 = true
        self.thirdView.backgroundColor = color
     }
     else if !check4
     {
        check4 = true
        self.fourthView.backgroundColor =  UIColor(named: "SecondaryColor")!
        if checkCode()
        {
           self.presentVC(identifier: "NavigationController")
        }
        else
        {
            restart()
        }
     }
    }
    
    
    func checkCode()->Bool
    {
       if let code = UserDefaults.standard.object(forKey: "code") as? String
       {
         if code == self.code
         {
            Drop.down("Unlocked successfully", state: .success, duration: 1.0)
            return true
        }
        Drop.down("Incorrect code", state: .error, duration: 1.0)
        return false
       }
        else
       {
         Drop.down("Incorrect code", state: .error, duration: 1.0)
         return false
       }
    
    }
    
    func restart()
    {
         self.firstView.backgroundColor = .clear
         self.secondView.backgroundColor = .clear
         self.thirdView.backgroundColor = .clear
         self.fourthView.backgroundColor = .clear
         self.check1 = false
         self.check2 = false
         self.check3 = false
         self.check4 = false
        code = ""
    
    }
    
    //MARK:- Event handler
    @IBAction func oneBtn(_ sender: Any)
    {
        code.append("1")
        updateView()
    }
    @IBAction func twoBtn(_ sender: Any)
    {
         code.append("2")
         updateView()
    }
    @IBAction func threeBtn(_ sender: Any)
    {
         code.append("3")
         updateView()
    }
    @IBAction func fourBtn(_ sender: Any)
    {
         code.append("4")
         updateView()
    }
    @IBAction func fiveBtn(_ sender: Any)
    {
         code.append("5")
         updateView()
    }
    @IBAction func sixBtn(_ sender: Any)
    {
         code.append("6")
         updateView()
    }
    @IBAction func sevenBtn(_ sender: Any)
    {
         code.append("7")
         updateView()
    }
    @IBAction func eightBtn(_ sender: Any)
    {
         code.append("8")
         updateView()
    }
    @IBAction func nineBtn(_ sender: Any)
    {
         code.append("9")
         updateView()
    }
    @IBAction func zeroBtn(_ sender: Any)
    {
         code.append("0")
         updateView()
    }
   
    @IBAction func didTapAddCode(_ sender: Any)
    {
        let popupView = Bundle.main.loadNibNamed("Code", owner: self, options: nil)?[0] as! Code
        popupView.setNeedsLayout()
        popupView.delegate = self
        self.presentPopupView(popupView)
    }
    @IBAction func didTapForgotCode(_ sender: Any)
    {
        let popupView = Bundle.main.loadNibNamed("ForgotCode", owner: self, options: nil)?[0] as! ForgotCode
        popupView.setNeedsLayout()
        popupView.delegate = self
        self.presentPopupView(popupView)
    }
    
    //MARK:- Done Delegate
    func doneBtnTapped()
    {
        self.dismissPopupView()
        self.presentVC(identifier: "NavigationController")
    }
    

}


extension CodeViewController : ForgotCodeDelegate
{
    func resetApp()
    {
        self.dismissPopupView()
        self.restart()
        self.stackView.isHidden = true
        self.numberView.isHidden = true
        self.forgotCode.isHidden = true
        self.addCodeView.isHidden = false
        self.label.isHidden = false
        self.titleView.backgroundColor = UIColor(named: "PrimaryColor")!
        DataHandler.sharedInstance.resetApp()
        
    }
    
    func presentAlert(alert: UIAlertController)
    {
        self.present(alert, animated: true, completion: nil)
    }
    
    func didTapdone()
    {
        self.dismissPopupView()
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            let popupView = Bundle.main.loadNibNamed("Code", owner: self, options: nil)?[0] as! Code
            popupView.setNeedsLayout()
            popupView.delegate = self
            self.presentPopupView(popupView)
        }
        
        
    }
    
    
    
}
