//
//  SettingsViewController.swift
//  DailyDiary
//
//  Created by Huda Jawed on 7/3/19.
//  Copyright © 2019 Huda Jawed. All rights reserved.
//

import UIKit
import STZPopupView
import SwiftyDrop
import UserNotifications

class SettingsViewController: UIViewController,ColorDelegate,Done {
    
    @IBOutlet weak var diaryUILabel: UILabel!
    @IBOutlet weak var notificationLabel: UILabel!
    @IBOutlet weak var helpLabel: UILabel!
    @IBOutlet weak var privacyLabel: UILabel!
    @IBOutlet weak var loggoutLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

         self.configureNavigationBar(title: "Settings", tintColor: .white, barColor: .white, isTransparent: true)
        
        if let notification = UserDefaults.standard.object(forKey: "notification") as? Bool
        {
            if notification
            {
                self.statusLabel.text = "ON"
                self.statusLabel.textColor = UIColor(hexString: "009051")
            }
        }
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.configureNavigationBar(title: "Settings", tintColor: .white, barColor: .white, isTransparent: true)
       
    }
    
    func showNotification()
    {
        let content = UNMutableNotificationContent()
        content.title = "Secret Diary"
        content.body = "Write your diary and save your memories"
        content.sound = UNNotificationSound.default
        
        let date = Date(timeIntervalSinceNow: 3600)
        let triggerDaily = Calendar.current.dateComponents([.hour,.minute,.second,], from: date)
        let trigger = UNCalendarNotificationTrigger(dateMatching: triggerDaily, repeats: true)
        
        let request = UNNotificationRequest(identifier: "identifier", content: content, trigger: trigger)
        
        UNUserNotificationCenter.current().add(request) { (error) in
            print(error?.localizedDescription)
        }
    }
    
    @IBAction func didTapThemeColor(_ sender: Any)
    {
        let popupView = Bundle.main.loadNibNamed("Color", owner: self, options: nil)?[0] as! Color
        popupView.setNeedsLayout()
        popupView.delegate = self
        self.presentPopupView(popupView)
    }
    
    @IBAction func didTapDailyReminder(_ sender: Any)
    {
        if let isNotificationOn = UserDefaults.standard.object(forKey: "notification") as? Bool
        {
            if isNotificationOn
            {
                Drop.down("Daily notification for this app has turned off", state: .success, duration: 1.0)
                UserDefaults.standard.set(false, forKey: "notification")
                UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
                statusLabel.text = "OFF"
                statusLabel.textColor = UIColor(hexString: "941100")
            }
            else
            {
              Drop.down("Daily notification for this app has turned on", state: .success, duration: 1.0)
              UserDefaults.standard.set(true, forKey: "notification")
                showNotification()
                statusLabel.text = "ON"
                statusLabel.textColor = UIColor(hexString: "009051")
            }
        }
        else
        {
            Drop.down("Daily notification for this app has turned on", state: .success, duration: 1.0)
            UserDefaults.standard.set(true, forKey: "notification")
            showNotification()
            statusLabel.text = "ON"
            statusLabel.textColor = UIColor(hexString: "009051")
        }
    }
    
    @IBAction func didTapChangeCode(_ sender: Any)
    {
        let popupView = Bundle.main.loadNibNamed("Code", owner: self, options: nil)?[0] as! Code
        popupView.setNeedsLayout()
        popupView.isFromSetting = true
        popupView.delegate = self
        popupView.codeTextField.placeholder = "Enter previous code"
       popupView.confirmCodeTextField.placeholder = "Enter new code"
        self.presentPopupView(popupView)
    }
    
    @IBAction func didTapSecurityQuestion(_ sender: Any)
    {
        if let ques1 = UserDefaults.standard.object(forKey: "ques1") as? String
        {
        let alert = UIAlertController(title: "Alert", message: "You have already added security questions!", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler:nil))
        self.present(alert, animated: true, completion: nil)
        }
        else
        {
         self.pushVC(identifier: "SecurityQuestionsViewController")
        }
    }
    
    @IBAction func didTapAboutIUs(_ sender: Any)
    {
        self.pushVC(identifier: "AboutUsViewController")
    }
    
    func getColor(color: String)
    {
        UserDefaults.standard.set(color, forKey: "color")
        self.dismissPopupView()
          self.configureNavigationBar(title: "Settings", tintColor: .white, barColor: .white, isTransparent: true)
    }
    
    func doneBtnTapped()
    {
        Drop.down("Code has changed successfully", state: .success, duration: 1.0)
        self.dismissPopupView()
    }
    
    @IBAction func didTapLockDiary(_ sender: Any)
    {
      self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func didTapResetApp(_ sender: Any)
    {
        let alert = UIAlertController(title: "Reset", message: "Are you sure you want to reset your app?", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler:nil))
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            
            Drop.down("App has reset", state: .success, duration: 1.0)
            UserDefaults.standard.set(nil, forKey: "code")
            UserDefaults.standard.set(nil, forKey: "color")
            UserDefaults.standard.set(nil, forKey: "ques1")
            UserDefaults.standard.set(nil, forKey: "ques2")
            UserDefaults.standard.set(nil, forKey: "ans1")
            UserDefaults.standard.set(nil, forKey: "ans2")
            UserDefaults.standard.set(nil, forKey: "notification")
            UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
            DataHandler.sharedInstance.resetApp()
            self.dismiss(animated: true, completion: nil)
        }))
        self.present(alert, animated: true, completion: nil)
        
    }
    
}
