//
//  SecurityQuestionsViewController.swift
//  DailyDiary
//
//  Created by Huda Jawed on 7/4/19.
//  Copyright © 2019 Huda Jawed. All rights reserved.
//

import UIKit
import DropDown
import SwiftyDrop

class SecurityQuestionsViewController: UIViewController {

    @IBOutlet weak var ques1TextField: CustomTextField!
    @IBOutlet weak var ques2TextField: CustomTextField!
    @IBOutlet weak var ans1TextField: CustomTextField!
    @IBOutlet weak var ans2TextField: CustomTextField!
    
    let ques1Array = ["What was the name of your first pet?","What is the name of your best friend in high school?","Where did you go the first time you flew on a plane?","In what city did your parents meet?"]
    let ques2Array = ["What is your dream job?","What was your childhood nickname?","What was the model of your first car?","What is your favorite children's book?"]
    
    var dropDown = DropDown()
    var dropDown2 = DropDown()
    
    override func viewDidLoad() {
        super.viewDidLoad()

         self.configureNavigationBar(title: "Security Questions", tintColor: .white, barColor: UIColor(named: "PrimaryColor")!, isTransparent: true)
        
        DropDown.configureDropDown(dropDown: dropDown, view: self.ques1TextField)
        DropDown.configureDropDown(dropDown: dropDown2, view: self.ques2TextField)
        
    }
    
    @IBAction func didTapQues1(_ sender: Any)
    {
        self.dropDown.dataSource = self.ques1Array
        self.dropDown.show()
        
        self.dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.ques1TextField.text = self.ques1Array[index]
            self.dropDown.hide()
        }
    }
    
    @IBAction func didTapQues2(_ sender: Any)
    {
        self.dropDown2.dataSource = self.ques2Array
        self.dropDown2.show()
        
        self.dropDown2.selectionAction = { [unowned self] (index: Int, item: String) in
            self.ques2TextField.text = self.ques2Array[index]
            self.dropDown2.hide()
        }
    }
    
    @IBAction func didTapSaveBtn(_ sender: Any)
    {
        if ques1TextField.text! == "" || ques2TextField.text == "" || ans1TextField.text == "" || ans2TextField.text == ""
        {
            Drop.down("Kindly select both questions and answer them", state: .error, duration: 1.0)
            return
        }
        else
        {
            UserDefaults.standard.set(ques1TextField.text!, forKey: "ques1")
            UserDefaults.standard.set(ques2TextField.text!, forKey: "ques2")
            UserDefaults.standard.set(ans1TextField.text!, forKey: "ans1")
            UserDefaults.standard.set(ans2TextField.text!, forKey: "ans2")
            Drop.down("Security questions have added successfully", state: .success, duration: 1.0)
    
        self.navigationController?.popViewController(animated: true)
        }
    }
    
}
