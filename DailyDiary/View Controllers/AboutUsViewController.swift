//
//  AboutUsViewController.swift
//  DailyDiary
//
//  Created by Huda Jawed on 7/4/19.
//  Copyright © 2019 Huda Jawed. All rights reserved.
//

import UIKit

class AboutUsViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

         self.configureNavigationBar(title: "About Us", tintColor: .white, barColor: UIColor(named: "PrimaryColor")!, isTransparent: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.configureNavigationBar(title: "About Us", tintColor: .white, barColor: UIColor(named: "PrimaryColor")!, isTransparent: true)
     
    }
    

 
}
