//
//  ForgotCode.swift
//  DailyDiary
//
//  Created by Huda Jawed on 7/4/19.
//  Copyright © 2019 Huda Jawed. All rights reserved.
//

import UIKit
import SwiftyDrop
import UserNotifications

protocol ForgotCodeDelegate
{
    func didTapdone()
    func resetApp()
    func presentAlert(alert:UIAlertController)
}


class ForgotCode: UIView {

    @IBOutlet weak var select: UILabel!
    @IBOutlet weak var ques1: UILabel!
    @IBOutlet weak var ques2: UILabel!
    @IBOutlet weak var ans2: UITextField!
    @IBOutlet weak var ans1: UITextField!
    @IBOutlet weak var cancel: UIButton!
    @IBOutlet weak var done: UIButton!
    var haveQues = true
    
    var delegate : ForgotCodeDelegate?
    @IBOutlet weak var securityQuestionsView: UIView!
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        securityQuestionsView.isHidden = true
        self.layer.cornerRadius = 10.0
        
        if let color = UserDefaults.standard.object(forKey: "color") as? String
        {
            let appColor = UIColor(hexString: color)
           // select.textColor = appColor
            done.setTitleColor(appColor, for: .normal)
            cancel.setTitleColor(appColor, for: .normal)
        }
        else
        {
           // select.textColor = UIColor(named: "PrimaryColor")!
            done.setTitleColor(UIColor(named: "PrimaryColor")!, for: .normal)
            cancel.setTitleColor(UIColor(named: "PrimaryColor")!, for: .normal)
        }

        
        if let q1 = UserDefaults.standard.object(forKey: "ques1") as? String, let q2 = UserDefaults.standard.object(forKey: "ques2") as? String
        {
          self.ques1.text = q1
          self.ques2.text = q2
        
        }
        else
        {
            self.ques1.isHidden = true
            self.ans1.isHidden = true
            self.ans2.isHidden = true
            self.ques2.text = "YOU HAVE NOT ADDED ANY SECURITY QUESTIONS!"
            haveQues = false
        }
        
        
    }
    @IBAction func didTapAnswerSecurityQuestions(_ sender: Any)
    {
        securityQuestionsView.isHidden = false
    }
    @IBAction func didTapResetApp(_ sender: Any)
    {
        let alert = UIAlertController(title: "Reset", message: "Are you sure you want to reset your app?", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler:nil))
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            
            Drop.down("App has reset", state: .success, duration: 1.0)
            UserDefaults.standard.set(nil, forKey: "color")
            UserDefaults.standard.set(nil, forKey: "code")
            UserDefaults.standard.set(nil, forKey: "ques1")
            UserDefaults.standard.set(nil, forKey: "ques2")
            UserDefaults.standard.set(nil, forKey: "ans1")
            UserDefaults.standard.set(nil, forKey: "ans2")
            UserDefaults.standard.set(nil, forKey: "notification")
            UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
            self.delegate?.resetApp()
            }))
        self.delegate?.presentAlert(alert: alert)
       
    }

    @IBAction func didTapCancelBtn(_ sender: Any)
    {
         securityQuestionsView.isHidden = true
    }
    
    @IBAction func didTapDoneBtn(_ sender: Any)
    {
        if !haveQues
        {
             securityQuestionsView.isHidden = true
        }
        else
        {
        if let a1 = UserDefaults.standard.object(forKey: "ans1") as? String, let a2 = UserDefaults.standard.object(forKey: "ans2") as? String
        {
        if (ans1.text)?.lowercased() == a1.lowercased() && (ans2.text)?.lowercased() == a2.lowercased()
        {
              self.delegate?.didTapdone()
        }
            else
        {
            Drop.down("Answers are not correct", state: .error, duration: 1.0)
            }
        }
        }
      
    }
    
}

