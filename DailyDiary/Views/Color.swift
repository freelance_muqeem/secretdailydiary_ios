//
//  Color.swift
//  DailyDiary
//
//  Created by Huda Jawed on 7/3/19.
//  Copyright © 2019 Huda Jawed. All rights reserved.
//

import UIKit

protocol ColorDelegate {
    func getColor(color:String)
}

class Color: UIView {

    var colors = ["000000","FF6597","B370FF","78FFCB","FFDF46","998AFF","08FFEE","FFAE1B","FF5B5E"]
    var delegate : ColorDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        self.layer.cornerRadius = 5.0
    }
    
   
    @IBAction func red(_ sender: Any)
    {
        self.delegate?.getColor(color: self.colors[8])
    }
    @IBAction func orange(_ sender: Any)
    {
        self.delegate?.getColor(color: self.colors[7])
    }
    
    @IBAction func yellow(_ sender: Any)
    {
        self.delegate?.getColor(color: self.colors[4])
    }
    @IBAction func green(_ sender: Any)
    {
        self.delegate?.getColor(color: self.colors[3])
    }
  
    @IBAction func blue(_ sender: Any)
    {
        self.delegate?.getColor(color: self.colors[6])
    }
    @IBAction func turquoise(_ sender: Any)
    {
        self.delegate?.getColor(color: self.colors[0])
    }
    @IBAction func lightPurple(_ sender: Any)
    {
        self.delegate?.getColor(color: self.colors[5])
    }
    @IBAction func pink(_ sender: Any)
    {
        self.delegate?.getColor(color: self.colors[1])
    }
 
    @IBAction func purple(_ sender: Any)
    {
        self.delegate?.getColor(color: self.colors[2])
    }
   
}
