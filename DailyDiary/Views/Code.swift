//
//  Code.swift
//  DailyDiary
//
//  Created by Huda Jawed on 7/3/19.
//  Copyright © 2019 Huda Jawed. All rights reserved.
//

import UIKit
import SwiftyDrop

protocol Done
{
    func doneBtnTapped()
}

class Code: UIView,UITextFieldDelegate {

    @IBOutlet weak var doneBtn: UIButton!
    @IBOutlet weak var confirmCodeTextField: UITextField!
    @IBOutlet weak var codeTextField: UITextField!
    var delegate : Done?
    var isFromSetting = false
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        self.layer.cornerRadius = 5.0
        confirmCodeTextField.delegate = self
        codeTextField.delegate = self
    }

    
    @IBAction func didTapDoneBtn(_ sender: Any) {
        
        if isFromSetting
        {
            if let code = UserDefaults.standard.object(forKey: "code") as? String
            {
                if code != self.codeTextField.text
                {
                     Drop.down("You have entered wrong code,Try again", state: .error, duration: 1.0)
                    return
                }
                else if codeTextField.text == "" || confirmCodeTextField.text == ""
                {
                    Drop.down("Enter code", state: .error, duration: 1.0)
                    return
                }
                else if (codeTextField.text?.count)! < 4 || (confirmCodeTextField.text?.count)! < 4
                {
                    Drop.down("Kindly enter 4 digit code", state: .error, duration: 1.0)
                    return
                }
                else
                {
                    Drop.down("Code has changed successfully", state: .error, duration: 1.0)
                    UserDefaults.standard.set(confirmCodeTextField.text!, forKey: "code")
                    self.delegate?.doneBtnTapped()
                    return
                }
                
            }
        }
       
        if codeTextField.text == "" || confirmCodeTextField.text == ""
        {
            Drop.down("Enter code", state: .error, duration: 1.0)
            return
        }
        if codeTextField.text != confirmCodeTextField.text
        {
            Drop.down("Code does not match", state: .error, duration: 1.0)
            codeTextField.text = ""
            confirmCodeTextField.text = ""
            return
        }
        if (codeTextField.text?.count)! < 4 || (confirmCodeTextField.text?.count)! < 4
        {
            Drop.down("Kindly enter 4 digit code", state: .error, duration: 1.0)
            return
        }
       
        UserDefaults.standard.set(codeTextField.text!, forKey: "code")
            self.delegate?.doneBtnTapped()
    }
    
    //MARK:- TextField Delegate
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let maxLength = 4
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
    }
    
}
