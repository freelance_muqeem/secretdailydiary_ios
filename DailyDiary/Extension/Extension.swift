//
//  Extension.swift
//  DailyDiary
//
//  Created by Huda Jawed on 7/3/19.
//  Copyright © 2019 Huda Jawed. All rights reserved.
//

import UIKit
import DropDown

extension UIViewController
{
func configureNavigationBar(title : String , tintColor: UIColor, barColor: UIColor, isTransparent: Bool)
{
    self.navigationItem.title =  title
    self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: tintColor]
    if let color = UserDefaults.standard.object(forKey: "color") as? String
    {
         self.navigationController?.navigationBar.backgroundColor = UIColor(hexString: color)
    }
    else
    {
        self.navigationController?.navigationBar.backgroundColor = UIColor(named: "PrimaryColor")!
    }
    self.navigationController?.navigationBar.tintColor = tintColor
    
    if isTransparent
    {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
    }
}
    
    func pushVC(identifier:String)
    {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: identifier)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func presentVC(identifier:String)
    {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: identifier)
        self.present(vc, animated: true, completion:nil)
    }
    

}

extension UIColor {
    convenience init(hexString: String, alpha: CGFloat = 1.0) {
        let hexString: String = hexString.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let scanner = Scanner(string: hexString)
        if (hexString.hasPrefix("#")) {
            scanner.scanLocation = 1
        }
        var color: UInt32 = 0
        scanner.scanHexInt32(&color)
        let mask = 0x000000FF
        let r = Int(color >> 16) & mask
        let g = Int(color >> 8) & mask
        let b = Int(color) & mask
        let red   = CGFloat(r) / 255.0
        let green = CGFloat(g) / 255.0
        let blue  = CGFloat(b) / 255.0
        self.init(red:red, green:green, blue:blue, alpha:alpha)
    }
    func toHexString() -> String {
        var r:CGFloat = 0
        var g:CGFloat = 0
        var b:CGFloat = 0
        var a:CGFloat = 0
        getRed(&r, green: &g, blue: &b, alpha: &a)
        let rgb:Int = (Int)(r*255)<<16 | (Int)(g*255)<<8 | (Int)(b*255)<<0
        return String(format:"#%06x", rgb)
}
}

extension DropDown
{
    static func configureDropDown(dropDown: DropDown, view : AnchorView)
    {
        dropDown.dismissMode = .onTap
        dropDown.anchorView = view
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.backgroundColor = UIColor.white
        dropDown.textFont = UIFont(name: "HelveticaNeue", size: 12.0)!
        dropDown.cellHeight = 33
    }
}
   
