//
//  DataHandler.swift
//  DailyDiary
//
//  Created by Huda Jawed on 7/3/19.
//  Copyright © 2019 Huda Jawed. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class DataHandler : NSObject
{
    static let sharedInstance = DataHandler()
    private let context : NSManagedObjectContext!
    
    private override init()
    {
        context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        super.init()
    }
    
    
    func saveStory(title: String,mood:String,detail:String,color:String,images:[Data])
    {
        let df = DateFormatter()
        df.dateFormat = "yyyy-MMMM-dd"
        let str = df.string(from: Date())
        let now = str.split(separator: "-")
        let date = String(now[2])
        let month = String(now[1])
        let year = String(now[0])
        let id = UUID().uuidString
        
        let df2 = DateFormatter()
        df2.dateFormat = "yyyy-MMMM-dd hh:mm:ss"
        let str2 = df2.string(from: Date())
        let time = String(str2.split(separator: " ")[1])
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE"
        let dayInWeek = dateFormatter.string(from: Date())
        
         let entity =
                NSEntityDescription.entity(forEntityName: "Story",
                                           in: self.context)!
            
            let words = NSManagedObject(entity: entity,
                                        insertInto: self.context)
            
            words.setValue(title, forKeyPath: "title")
            words.setValue(mood, forKey: "mood")
            words.setValue(detail, forKey: "detail")
            words.setValue(dayInWeek, forKey: "day")
            words.setValue(date, forKey: "date")
            words.setValue(month, forKey: "month")
            words.setValue(year, forKey: "year")
            words.setValue(Date(), forKey: "wholeDate")
            words.setValue(color, forKey: "color")
            words.setValue(id, forKey: "id")
            words.setValue(time, forKey: "time")
            
            do {
                try self.context.save()
            } catch let error as NSError {
                print("Could not save. \(error), \(error.userInfo)")
            }
        
        for i in 0..<images.count
        {
            self.saveImages(image: images[i], index: String(i), id: id)
        }
        
        
            
        }
    
    
    private func saveImages(image:Data,index:String,id:String)
    {
        let entity =
            NSEntityDescription.entity(forEntityName: "Images",
                                       in: self.context)!
        
        let words = NSManagedObject(entity: entity,
                                    insertInto: self.context)
        
        words.setValue(image, forKeyPath: "image")
        words.setValue(index, forKey: "index")
        words.setValue(id, forKey: "id")
        
        do {
            try self.context.save()
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }

    }
    
    func getStories(month:String)->[Dictionary<String,Any>]?
    {
        let request : NSFetchRequest =  NSFetchRequest<NSManagedObject>(entityName: "Story")
        request.predicate  = NSPredicate(format: "month == %@", month)
        
        var response = [Dictionary<String,Any>]()
        do{
            let results = try self.context.fetch(request)
            if(results.count > 0)
            {
                for data in results
                {
                    var dic = Dictionary<String,Any>()
                    dic["title"] = data.value(forKey: "title") as! String
                    dic["mood"] = data.value(forKey: "mood") as! String
                    dic["detail"] = data.value(forKey: "detail") as! String
                    dic["day"] = data.value(forKey: "day") as! String
                    dic["date"] = data.value(forKey: "date") as! String
                    dic["month"] = data.value(forKey: "month") as! String
                    dic["year"] = data.value(forKey: "year") as! String
                    dic["color"] = data.value(forKey: "color") as! String
                    dic["wholeDate"] = data.value(forKey: "wholeDate") as! Date
                    dic["id"] = data.value(forKey: "id") as! String
                    dic["time"] = data.value(forKey: "time") as! String
                    response.append(dic)
                }
                 let sortedArray = response.sorted{ ($0["wholeDate"])! as! Date > ($1["wholeDate"])! as! Date }
                return sortedArray
            }
            else
            {
                return nil
            }
        }catch
        {
            print("Error in fetching the result ")
            return nil
        }
    }
    
    
    func getImages(id:String)->[Dictionary<String,Any>]?
    {
        let request : NSFetchRequest =  NSFetchRequest<NSManagedObject>(entityName: "Images")
        request.predicate  = NSPredicate(format: "id == %@", id)
        
        var response = [Dictionary<String,Any>]()
        do{
            let results = try self.context.fetch(request)
            if(results.count > 0)
            {
                for data in results
                {
                    var dic = Dictionary<String,Any>()
                    dic["image"] = data.value(forKey: "image") as! Data
                    dic["index"] = data.value(forKey: "index") as! String
                   dic["id"] = data.value(forKey: "id") as! String
                  
                    response.append(dic)
                }
                return response
            }
            else
            {
                return nil
            }
        }catch
        {
            print("Error in fetching the result ")
            return nil
        }
    }
    
    func deleteStory(id:String)
    {
        let entities = ["Story","Images"]
        for entity in entities
        {

        let request = NSFetchRequest<NSFetchRequestResult>(entityName: entity)
            request.predicate  = NSPredicate(format: "id == %@", id)
        do
        {
            let result = try context.fetch(request)
           
            if result.count > 0
            {
                for r in result
                {
                    let manage = r as! NSManagedObject
                    context.delete(manage)
                    try context.save()
                }
                print("Story Deleted")
            }
            
        }catch{}
        }
    }
    
    func resetApp()
    {
        let entities = ["Story","Images"]
        for entity in entities
        {

            let request = NSFetchRequest<NSFetchRequestResult>(entityName: entity)
            do
            {
                let result = try context.fetch(request)
                if result.count > 0
                {
                    for r in result
                    {
                    let manage = r as! NSManagedObject
                    context.delete(manage)
                        try context.save()
                    }
                    print("Record Deleted")
                }
                
            }catch{}
    }
}
}
